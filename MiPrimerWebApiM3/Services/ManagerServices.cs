﻿using Microsoft.EntityFrameworkCore;
using MiPrimerWebApiM3.Contexts;
using MiPrimerWebApiM3.Entities;
using MiPrimerWebApiM3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Services
{
    public class ManagerServices : IManager
    {
        private readonly ApplicationDbContext _context;
        public ManagerServices(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TbManager>> GetAllAsync()
        {
            return await _context.TbManager.ToListAsync();
        }

        public async Task<TbManager> GetManagerByIdAsync(int id)
        {
            return await _context.TbManager.FindAsync(id);
        }

        public async Task CreateAsync(TbManager manager)
        {
            _context.TbManager.Add(manager);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var tbManager = await _context.TbManager.FindAsync(id);

            _context.TbManager.Remove(tbManager);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TbManager manager)
        {
            _context.Entry(manager).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
