USE [DataBaseTest]
GO
/****** Object:  Table [dbo].[tbAcuerdo]    Script Date: 01-08-2020 2:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAcuerdo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdManager] [int] NOT NULL,
	[CODIGO_PRODUCTO] [varchar](50) NOT NULL,
	[FechaVigenciaAcuerdo] [datetime] NOT NULL,
	[RebateAnual] [decimal](18, 0) NULL,
	[TipoPago] [char](1) NULL,
	[TipoRebate] [char](1) NULL,
	[ManagementFree] [decimal](18, 0) NULL,
	[AcuerdoComercial] [decimal](18, 0) NULL,
	[FechaHoraCambio] [datetime] NULL,
	[UsuarioReg] [varchar](100) NULL,
	[NemoBaseComercial] [varchar](100) NULL,
	[CodigoInterno] [varchar](50) NULL,
 CONSTRAINT [PK_tbAcuerdo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbComisionDiaria]    Script Date: 01-08-2020 2:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbComisionDiaria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdAcuerdo] [int] NOT NULL,
	[PatrimonioFondo] [decimal](18, 0) NOT NULL,
	[Comision] [decimal](18, 0) NOT NULL,
	[FechaHoraCalculo] [datetime] NOT NULL,
 CONSTRAINT [PK_tbComisionDiaria] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbManager]    Script Date: 01-08-2020 2:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbManager](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NOT NULL,
	[Plataforma] [char](1) NOT NULL,
	[Contacto] [varchar](250) NULL,
	[Email] [varchar](250) NULL,
	[Telefono] [varchar](50) NULL,
	[FechaContrato] [datetime] NULL,
 CONSTRAINT [PK_tbManager] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbAcuerdo]  WITH CHECK ADD  CONSTRAINT [FK_tbAcuerdo_ActivosLV] FOREIGN KEY([CODIGO_PRODUCTO])
REFERENCES [dbo].[ActivosLV] ([CODIGO_PRODUCTO])
GO
ALTER TABLE [dbo].[tbAcuerdo] CHECK CONSTRAINT [FK_tbAcuerdo_ActivosLV]
GO
ALTER TABLE [dbo].[tbAcuerdo]  WITH CHECK ADD  CONSTRAINT [FK_tbAcuerdo_tbManager] FOREIGN KEY([IdManager])
REFERENCES [dbo].[tbManager] ([Id])
GO
ALTER TABLE [dbo].[tbAcuerdo] CHECK CONSTRAINT [FK_tbAcuerdo_tbManager]
GO
ALTER TABLE [dbo].[tbComisionDiaria]  WITH CHECK ADD  CONSTRAINT [FK_tbComisionDiaria_tbAcuerdo] FOREIGN KEY([IdAcuerdo])
REFERENCES [dbo].[tbAcuerdo] ([Id])
GO
ALTER TABLE [dbo].[tbComisionDiaria] CHECK CONSTRAINT [FK_tbComisionDiaria_tbAcuerdo]
GO
