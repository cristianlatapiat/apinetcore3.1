﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiPrimerWebApiM3.Contexts;
using MiPrimerWebApiM3.Entities;
using MiPrimerWebApiM3.Interfaces;
using MiPrimerWebApiM3.Models;

namespace MiPrimerWebApiM3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManagersController : ControllerBase
    {
        private readonly IManager servicesManager;
        private readonly IMapper mapper;

        public ManagersController(IManager servicesManager, IMapper mapper)
        {
            this.servicesManager = servicesManager;
            this.mapper = mapper;
        }

        /// <summary>
        /// Obtiene el listado de Manager
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManagerDTO>>> GetTbManager()
        {

            IEnumerable<TbManager> result = await servicesManager.GetAllAsync();
            var managerDTO = mapper.Map<IEnumerable<ManagerDTO>>(result);
            return managerDTO.ToList();
        }


        // GET: api/Managers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ManagerDTO>> GetManagerById(int id)
        {
            var tbManager = await servicesManager.GetManagerByIdAsync(id);

            if (tbManager == null)
            {
                return NotFound();
            }
            ManagerDTO managerDTO = GetManagerDTO(tbManager);
            return managerDTO;
        }

        // PUT: api/Managers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, TbManager tbManager)
        {
            if (id != tbManager.Id)
            {
                return BadRequest();
            }

            try
            {
                await servicesManager.UpdateAsync(tbManager);
            }
            catch (DbUpdateConcurrencyException)
            {
                bool exist = await ManagerExists(id);
                if (!exist)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Managers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TbManager>> Create(TbManager tbManager)
        {
            await servicesManager.CreateAsync(tbManager);
            ManagerDTO managerDTO = GetManagerDTO(tbManager);

            return CreatedAtAction("GetManagerById", new { id = tbManager.Id }, managerDTO);
        }

   
        // DELETE: api/Managers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ManagerDTO>> Delete(int id)
        {
            var tbManager = await servicesManager.GetManagerByIdAsync(id);
            if (tbManager == null)
            {
                return NotFound();
            }

            await servicesManager.DeleteAsync(id);
            ManagerDTO managerDTO = GetManagerDTO(tbManager);
            return managerDTO;
        }

        private async Task<bool> ManagerExists(int id)
        {
            var result = await servicesManager.GetAllAsync();
            return result.Any(e => e.Id == id);

        }
        private ManagerDTO GetManagerDTO(TbManager tbManager)
        {
            return mapper.Map<ManagerDTO>(tbManager);
        }

    }
}
