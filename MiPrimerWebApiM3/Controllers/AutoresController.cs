﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MiPrimerWebApiM3.Contexts;
using MiPrimerWebApiM3.Entities;

namespace MiPrimerWebApiM3.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AutoresController : Controller
    {
        private readonly ApplicationDbContext db;

        public AutoresController(ApplicationDbContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Autor>> Get()
        {
            return db.Autores.Include(a => a.Libros).ToList();
        }

        /*
         * regla de ruteo autores/first convinando el Route mas lo indicado en atributo
         * si comienzo con / ignoro lo indicado en el Route
         */
        [HttpGet("First")]
        public ActionResult<Autor> GetFirst()
        {
            return db.Autores.Include(a => a.Libros).FirstOrDefault();
        }

        [HttpGet("{id}", Name = "ObtenerAutor")]
        public ActionResult<Autor> Get(int id)
        {
            var autor = db.Autores.Include(a => a.Libros).FirstOrDefault(f => f.Id == id);
            if (autor == null)
                return NotFound();
            return autor;
        }

        [HttpPost]
        public ActionResult Post([FromBody] Autor autor)
        {
            db.Autores.Add(autor);
            db.SaveChanges();
            return new CreatedAtRouteResult("ObtenerAutor", new { id = autor.Id }, autor);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Autor autor)
        {
            if (id != autor.Id)
            {
                return BadRequest();
            }
            db.Entry(autor).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }


        [HttpDelete("{id}")]
        public ActionResult<Autor> Delete(int id)
        {
            var autor = db.Autores.FirstOrDefault(a => a.Id == id);

            if (autor == null)
            {
                return NotFound();
            }
            db.Autores.Remove(autor);
            db.SaveChanges();
            return autor;
        }
    }
}
