﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiPrimerWebApiM3.Contexts;
using MiPrimerWebApiM3.Entities;

namespace MiPrimerWebApiM3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AcuerdosController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public AcuerdosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Acuerdos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TbAcuerdo>>> GetTbAcuerdo()
        {
            return await _context.TbAcuerdo.ToListAsync();
        }

        // GET: api/Acuerdos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TbAcuerdo>> GetTbAcuerdo(int id)
        {
            var tbAcuerdo = await _context.TbAcuerdo.FindAsync(id);

            if (tbAcuerdo == null)
            {
                return NotFound();
            }

            return tbAcuerdo;
        }

        // PUT: api/Acuerdos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTbAcuerdo(int id, TbAcuerdo tbAcuerdo)
        {
            if (id != tbAcuerdo.Id)
            {
                return BadRequest();
            }

            _context.Entry(tbAcuerdo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbAcuerdoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Acuerdos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TbAcuerdo>> PostTbAcuerdo(TbAcuerdo tbAcuerdo)
        {
            _context.TbAcuerdo.Add(tbAcuerdo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTbAcuerdo", new { id = tbAcuerdo.Id }, tbAcuerdo);
        }

        // DELETE: api/Acuerdos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TbAcuerdo>> DeleteTbAcuerdo(int id)
        {
            var tbAcuerdo = await _context.TbAcuerdo.FindAsync(id);
            if (tbAcuerdo == null)
            {
                return NotFound();
            }

            _context.TbAcuerdo.Remove(tbAcuerdo);
            await _context.SaveChangesAsync();

            return tbAcuerdo;
        }

        private bool TbAcuerdoExists(int id)
        {
            return _context.TbAcuerdo.Any(e => e.Id == id);
        }
    }
}
