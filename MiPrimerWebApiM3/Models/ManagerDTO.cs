﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Models
{
    public class ManagerDTO
    {
        /// <summary>
        /// Nombre Manager
        /// </summary>
        [Required]
        [MaxLength(250)]
        public string Nombre { get; set; }
        /// <summary>
        /// Plataforma Internacional o Local
        /// </summary>
        [Required]
        [MaxLength(1)]
        public string Plataforma { get; set; }
        /// <summary>
        /// Contacto
        /// </summary>
        public string Contacto { get; set; }

        public string Email { get; set; }
        public string Telefono { get; set; }
        public DateTime? FechaContrato { get; set; }

    }
}
