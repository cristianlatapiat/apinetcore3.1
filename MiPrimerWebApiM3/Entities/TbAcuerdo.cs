﻿using System;
using System.Collections.Generic;

namespace MiPrimerWebApiM3.Entities
{
    public partial class TbAcuerdo
    {
        public TbAcuerdo()
        {
            TbComisionDiaria = new HashSet<TbComisionDiaria>();
        }

        public int Id { get; set; }
        public int IdManager { get; set; }
        public string CodigoProducto { get; set; }
        public DateTime FechaVigenciaAcuerdo { get; set; }
        public decimal? RebateAnual { get; set; }
        public string TipoPago { get; set; }
        public string TipoRebate { get; set; }
        public decimal? ManagementFree { get; set; }
        public decimal? AcuerdoComercial { get; set; }
        public DateTime? FechaHoraCambio { get; set; }
        public string UsuarioReg { get; set; }
        public string NemoBaseComercial { get; set; }
        public string CodigoInterno { get; set; }

        public virtual ActivosLv CodigoProductoNavigation { get; set; }
        public virtual TbManager IdManagerNavigation { get; set; }
        public virtual ICollection<TbComisionDiaria> TbComisionDiaria { get; set; }
    }
}
