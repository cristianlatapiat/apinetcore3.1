﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MiPrimerWebApiM3.Entities
{
    public partial class TbManager
    {
        public TbManager()
        {
            TbAcuerdo = new HashSet<TbAcuerdo>();
        }

        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(1)]
        public string Plataforma { get; set; }
        public string Contacto { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public DateTime? FechaContrato { get; set; }

        public virtual ICollection<TbAcuerdo> TbAcuerdo { get; set; }
    }
}
