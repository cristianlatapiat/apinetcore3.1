﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MiPrimerWebApiM3.Entities
{
    public partial class ActivosLv
    {
        public ActivosLv()
        {
            TbAcuerdo = new HashSet<TbAcuerdo>();
        }

        [Key]
        public string CodigoProducto { get; set; }
        public string Isin { get; set; }
        public string NombreActivo { get; set; }
        public int? CategoriaId { get; set; }
        public string CatNombre { get; set; }
        public string AgrupadaId { get; set; }
        public string RegionId { get; set; }
        public string SectorId { get; set; }
        public string CategoriaAgrupada { get; set; }
        public string Sector { get; set; }
        public string Region { get; set; }
        public string TipoProducto { get; set; }
        public string Divisa { get; set; }
        public string BenchMarck { get; set; }
        public string BenchMarkNombre { get; set; }
        public string Fecha { get; set; }
        public decimal? Valorazion { get; set; }
        public string Origen { get; set; }
        public string NombreComercial { get; set; }
        public bool? Nuevo { get; set; }
        public bool? Activo { get; set; }
        public decimal? Aumtotal { get; set; }
        public int? NumClientes { get; set; }
        public decimal? NumCuotas { get; set; }
        public int? MonedaExpId { get; set; }
        public int? AssetClassId { get; set; }
        public string DescripcionActivo { get; set; }
        public string NemoBase { get; set; }

        public virtual ICollection<TbAcuerdo> TbAcuerdo { get; set; }
    }
}
