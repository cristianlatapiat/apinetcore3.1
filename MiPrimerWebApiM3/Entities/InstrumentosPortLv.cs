﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MiPrimerWebApiM3.Entities
{
    public partial class InstrumentosPortLv
    {
        [Key]
        public int InstrumentosPortLvId { get; set; }
        public int? PortafoliosLvId { get; set; }
        public string Cusip { get; set; }
        public string Isin { get; set; }
        public string NombreActivo { get; set; }
        public string Cartera { get; set; }
        public decimal? PesoRelativo { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public decimal? PrecioEstimado { get; set; }
        public decimal? PrecioObjetivo { get; set; }
        public decimal? PrecioReal { get; set; }
        public DateTime? FechaMod { get; set; }
        public bool? Estado { get; set; }
        public string CarteraId { get; set; }
    }
}
