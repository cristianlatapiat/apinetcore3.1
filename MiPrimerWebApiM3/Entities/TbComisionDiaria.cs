﻿using System;
using System.Collections.Generic;

namespace MiPrimerWebApiM3.Entities
{
    public partial class TbComisionDiaria
    {
        public int Id { get; set; }
        public int IdAcuerdo { get; set; }
        public decimal PatrimonioFondo { get; set; }
        public decimal Comision { get; set; }
        public DateTime FechaHoraCalculo { get; set; }

        public virtual TbAcuerdo IdAcuerdoNavigation { get; set; }
    }
}
