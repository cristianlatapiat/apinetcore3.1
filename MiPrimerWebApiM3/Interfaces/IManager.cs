﻿using MiPrimerWebApiM3.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Interfaces
{
    public interface IManager
    {
        Task<IEnumerable<TbManager>> GetAllAsync();
        Task<TbManager> GetManagerByIdAsync(int id);
        Task UpdateAsync(TbManager manager);
        Task CreateAsync(TbManager manager);
        Task DeleteAsync(int id);
    }
}
