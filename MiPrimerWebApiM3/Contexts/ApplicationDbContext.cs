﻿using Microsoft.EntityFrameworkCore;
using MiPrimerWebApiM3.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Autor> Autores { get; set; }
        public DbSet<Libro> Libros { get; set; }
        public DbSet<ActivosLv> ActivosLv { get; set; }
        public DbSet<InstrumentosPortLv> InstrumentosPortLv { get; set; }
        public DbSet<TbAcuerdo> TbAcuerdo { get; set; }
        public DbSet<TbComisionDiaria> TbComisionDiaria { get; set; }
        public DbSet<TbManager> TbManager { get; set; }


    }
}
